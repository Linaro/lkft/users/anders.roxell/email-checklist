# LKFT's email checklist

This is a short summary of what have to be done before fire out an email.

## checklist before sending out emails.

Check if anything have been upgraded since last build, like:
- [ ] config file
- [ ] Toolchain
- [ ] Base file system
- [ ] Test suite overlay
- [ ] QEMU

NOTE: if any changed, rebuild or retrigger tests with the older versions.

## Email report template

Verify before sending email:
- [ ] Reproducer scripts so we know they work.
- [ ] That all links are vaild


Be as elaborative as possible if applicable:
- [ ] Explain in detail why its a regression
- [ ] Show crashlogs (if applicable)
- [ ] Show tools used to build the kernel
- [ ] Show tuxmake/tuxrun reproducer (if applicable)
- [ ] show build/runtime logfile (if applicable)

Example1:
```
Todays next kernel tag next-20240701 failed to boot arm64 devices built with
gcc-13.  No boot output was produced.
This is one of the built config files [1]

We have started to bisect this issue and get back with updates.

Reported-by: Linux Kernel Functional Testing <lkft@linaro.org>


Tools versions:
ar/ld: ...
gcc: ...

To reproduce the build with tuxrun [2]:
tuxrun --runtime podman --device qemu-arm64 ...

To reproduce the build with tuxmake [3]:
tuxmake --runtime podman --target-arch arm64 --toolchain gcc-13 ...

Links:
[1] https://qa-reports.linaro.org/lkft/path/to/the/configfile
[2] https://path/to/tuxrun/repdoducer
[3] https://path/to/tuxmake/reproducer

--
Linaro LKFT
https://lkft.linaro.org
```

Example2:
```
Todays next kernel tag next-20240701 failed to boot an arm kernel on a
qemu-armv7 devices, that was built with toolchain gcc-13.  This is one of the
built config files [1]. The crashlog looks like this (full logfile [2]):

<snippet of a crashlog>


We have started to bisect this issue and get back with updates.

Reported-by: Linux Kernel Functional Testing <lkft@linaro.org>


Tools versions to build:
ar/ld: ...
gcc: ...

To reproduce the build with tuxrun [3]:
tuxrun --runtime podman --device qemu-armv7 ...

To reproduce the build with tuxmake [4]:
tuxmake --runtime podman --target-arch arm --toolchain gcc-13 ...

Links:
[1] https://qa-reports.linaro.org/lkft/path/to/the/configfile
[2] https://qa-reports.linaro.org/lkft/path/to/the/full/test/logfile
[3] https://path/to/tuxrun/repdoducer
[4] https://path/to/tuxmake/reproducer

--
Linaro LKFT
https://lkft.linaro.org
```
